const db = require('../db');
const Sequelize = require('sequelize');

module.exports = db.define('students', {
  name: Sequelize.STRING,
  grade: Sequelize.INTEGER
}, { timestamps: false });
