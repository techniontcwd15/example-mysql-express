const express = require('express');

const db = require('./db');
const Students = require('./models/students')
const app = express();

app.get('/', (req, res) => {
  db.query('SELECT * FROM students WHERE id=1',
  { type: db.QueryTypes.SELECT })
  .then(function(students) {
    res.json(students);
  })
});

app.get('/students', (req, res) => {
  Students.findAll({}).then(students => {
    res.json(students)
  });
});

app.listen(8080);
